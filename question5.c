#include <stdio.h>
int main() {
	int num,i,count;
	printf("Enter a positive integer :");
	scanf("%d", &num);
	for(i = 1; i <= num; i ++) {
		for(count = 1; count <= 10; count ++) {
			printf("%d * %d = %d\n", i, count, i * count);
		}
		printf("\n");
	}
}
